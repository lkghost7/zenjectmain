using System;
using System.Collections.Generic;
using System.Linq;

namespace ZenjectMain.Code.Core.Extensions
{
    public static class GameExtension
    {
        private static readonly Random _random = new Random();  
        public static void Shuffle<T>(this List<T> listShuffle)
        {
            for (int i = 0; i < listShuffle.Count; i++)
            {
                T temp = listShuffle[i];
                int randomIndex = UnityEngine.Random.Range(i, listShuffle.Count);
                listShuffle[i] = listShuffle[randomIndex];
                listShuffle[randomIndex] = temp;
            } 
        }
        
        public static void Shuffle<T>(this Stack<T> stack)
        {
            var values = stack.ToArray();
            stack.Clear();
            foreach (var value in values.OrderBy(x => _random.Next()))
                stack.Push(value);
        }
        
        public static void Shuffle<T>(this Queue<T> queue)
        {
            var values = queue.ToArray();
            queue.Clear();
            foreach (var value in values.OrderBy(x => _random.Next()))
                queue.Enqueue(value);
        }
    }
}