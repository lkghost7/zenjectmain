using System;
using ZenjectMain.Code.Infrastructure.StateMachine.States;
using ZenjectMain.Code.Infrastructure.StateMachine.StateSwitcher;

namespace ZenjectMain.Code.Core.UI.Gameplay.TopPanel
{
    public class TopPanel : IDisposable
    {
        private readonly TopPanelView _topPanelView;
        private readonly IStateSwitcher _stateSwitcher;

        public TopPanel(TopPanelView topPanelView,  IStateSwitcher stateSwitcher)
        {
            _topPanelView = topPanelView;
            _stateSwitcher = stateSwitcher;

            _topPanelView.OnExitClick += ExitToMainMenu;
        }

        public void Dispose() => _topPanelView.OnExitClick -= ExitToMainMenu;
        private void ExitToMainMenu() => _stateSwitcher.SwitchTo<MenuState>();
    }
}