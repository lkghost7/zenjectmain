using System;
using UnityEngine;
using UnityEngine.UI;
using ZenjectMain.Code.Core.UI.Settings;
using ZenjectMain.Code.Data.Enums;
using ZenjectMain.Code.Services.Sound;

namespace ZenjectMain.Code.Core.UI.MainMenu
{
    public class MainMenuView : MonoBehaviour
    {
        public event Action OnPlayButtonClick;
        
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _settingsButton;

        private SettingsView _settingsView;
        private ISoundService _soundService;
        
        public void Construct(ISoundService soundService) => _soundService = soundService;
        
        public void SetSettingsPanel(SettingsView settingsPanel)
        {
            _settingsView = settingsPanel;
        }

        private void Awake()
        {
            _playButton.onClick.AddListener(SendPlayButtonClick);
            _settingsButton.onClick.AddListener(SwitchSettingsPanel);
        }

        private void OnDestroy()
        {
            _playButton.onClick.RemoveListener(SendPlayButtonClick);
            _settingsButton.onClick.RemoveListener(SwitchSettingsPanel);
        }

        private void SwitchSettingsPanel()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            _settingsView.gameObject.SetActive(!_settingsView.gameObject.activeSelf);
        }

        private void SendPlayButtonClick()
        {
            _soundService.PlayEffectSound(SoundId.Click);
            OnPlayButtonClick?.Invoke();
        }
    }
}
