using ZenjectMain.Code.Data.Enums;
using ZenjectMain.Code.Data.Progress;
using ZenjectMain.Code.Data.StaticData.Sounds;

namespace ZenjectMain.Code.Services.Sound
{
    public interface ISoundService
    {
        void Construct(SoundData soundData, Settings userSettings);
        void PlayEffectSound(SoundId soundId);
        void SetBackgroundMusicVolume(float volume);
        void SetEffectsVolume(float volume);
        void PlayBackgroundMusic();
        void StopBackgroundMusic();
        bool MusicMuted { get; set; }
        bool EffectsMuted { get; set; }
    }
}