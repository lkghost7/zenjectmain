using ZenjectMain.Code.Infrastructure.StateMachine.States;

namespace ZenjectMain.Code.Services.StateFactory
{
    public interface IStateFactory
    {
        T Create<T>() where T : IExitableState;
    }
}