using Plugins.Zenject.Source.Main;
using ZenjectMain.Code.Infrastructure.StateMachine.States;

namespace ZenjectMain.Code.Services.StateFactory
{
    public class StatesFactory : IStateFactory
    {
        private readonly IInstantiator _instantiator;

        public StatesFactory(IInstantiator instantiator) =>
            _instantiator = instantiator;

        public T Create<T>() where T : IExitableState =>
            _instantiator.Instantiate<T>();
    }
}