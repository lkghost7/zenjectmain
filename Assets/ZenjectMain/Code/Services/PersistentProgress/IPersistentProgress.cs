using ZenjectMain.Code.Data.Progress;

namespace ZenjectMain.Code.Services.PersistentProgress
{
    public interface IPersistentProgress
    {
        PlayerProgress Progress { get; set; }
    }
}