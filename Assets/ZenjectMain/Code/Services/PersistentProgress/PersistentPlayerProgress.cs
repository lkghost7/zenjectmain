using ZenjectMain.Code.Data.Progress;

namespace ZenjectMain.Code.Services.PersistentProgress
{
    public class PersistentPlayerProgress : IPersistentProgress
    {
        public PlayerProgress Progress { get; set; }
    }
}