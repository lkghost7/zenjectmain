namespace ZenjectMain.Code.Services.UserBalance
{
    public interface IUserBalance
    {
        void Add(int balance);
        void Minus(int balance); 
    }
}