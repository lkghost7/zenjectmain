using ZenjectMain.Code.Services.PersistentProgress;
using ZenjectMain.Code.Services.SaveLoad;
using ZenjectMain.Code.Services.StaticData;

namespace ZenjectMain.Code.Services.UserBalance
{
    public class UserBalance : IUserBalance
    {
        private readonly IPersistentProgress _persistentProgress;
        private readonly ISaveLoad _saveLoadService;

        public UserBalance(IPersistentProgress persistentProgress, ISaveLoad saveLoadService, IStaticData staticData)
        {
            _persistentProgress = persistentProgress;
            _saveLoadService = saveLoadService;
        }

        public void Add(int balance)
        {
            _persistentProgress.Progress.Balance += balance;
            _saveLoadService.SaveProgress();
        }

        public void Minus(int balance)
        {
            _persistentProgress.Progress.Balance -= balance;
            _saveLoadService.SaveProgress();
        }
    }
}