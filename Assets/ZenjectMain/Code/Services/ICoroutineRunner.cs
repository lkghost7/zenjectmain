using System.Collections;
using UnityEngine;

namespace ZenjectMain.Code.Services
{
    public interface ICoroutineRunner
    {
        Coroutine StartCoroutine(IEnumerator routine);
    }
}