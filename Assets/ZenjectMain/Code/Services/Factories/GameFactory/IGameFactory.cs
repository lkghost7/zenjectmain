using ZenjectMain.Code.Services.CardMove;

namespace ZenjectMain.Code.Services.Factories.GameFactory
{
    public interface IGameFactory
    {
        Mover CreateCardMover();
    }
}