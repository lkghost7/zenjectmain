using ZenjectMain.Code.Services.CardMove;
using ZenjectMain.Code.Services.EntityContainer;
using ZenjectMain.Code.Services.Sound;
using ZenjectMain.Code.Services.StaticData;

namespace ZenjectMain.Code.Services.Factories.GameFactory
{
    public class GameFactory : IGameFactory
    {
        private readonly IStaticData _staticData;
        private readonly IEntityContainer _entityContainer;
        private readonly ISoundService _soundService;
        private readonly ICoroutineRunner _coroutineRunner;
        private readonly IMover _mover;
 
        public GameFactory(IStaticData staticData, IEntityContainer entityContainer, ISoundService soundService,
            ICoroutineRunner coroutineRunner, IMover mover)
        {
            _coroutineRunner = coroutineRunner;
            _staticData = staticData;
            _entityContainer = entityContainer;
            _soundService = soundService;
            _mover = mover;
        }

        public Mover CreateCardMover()
        {
            Mover mover = new Mover();
            _entityContainer.RegisterEntity(mover);
            return mover;
        }
    }
}