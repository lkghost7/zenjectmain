using UnityEngine;
using ZenjectMain.Code.Core.UI;
using ZenjectMain.Code.Core.UI.Gameplay.TopPanel;
using ZenjectMain.Code.Core.UI.MainMenu;
using ZenjectMain.Code.Core.UI.Settings;
using ZenjectMain.Code.Infrastructure.StateMachine.StateSwitcher;
using ZenjectMain.Code.Services.CardMove;
using ZenjectMain.Code.Services.EntityContainer;
using ZenjectMain.Code.Services.PersistentProgress;
using ZenjectMain.Code.Services.SaveLoad;
using ZenjectMain.Code.Services.Sound;
using ZenjectMain.Code.Services.StaticData;
using ZenjectMain.Code.Services.UserBalance;

namespace ZenjectMain.Code.Services.Factories.UIFactory
{ 
    public class UIFactory : IUIFactory
    {
        private readonly IPersistentProgress _playerProgress;
        private readonly ISoundService _soundService;
        private readonly IStaticData _staticData;
        private readonly IStateSwitcher _stateSwitcher;
        private readonly IEntityContainer _entityContainer;
        private readonly IUserBalance _userBalance;
        private readonly IMover _mover;
        private readonly IPersistentProgress _persistentProgress;
        private readonly ISaveLoad _saveLoad;

        public UIFactory(IEntityContainer entityContainer, IStaticData staticData,
            IStateSwitcher stateSwitcher, IPersistentProgress playerProgress,
            ISoundService soundService, IUserBalance userBalance, IMover mover, IPersistentProgress persistentProgress, ISaveLoad saveLoad)
        {
            _entityContainer = entityContainer;
            _playerProgress = playerProgress;
            _soundService = soundService;
            _userBalance = userBalance;
            _mover = mover;
            _persistentProgress = persistentProgress;
            _saveLoad = saveLoad;
            _staticData = staticData;
            _stateSwitcher = stateSwitcher;
        }

        public RootCanvas CreateRootCanvas()
        {
            RootCanvas root = Object.Instantiate(_staticData.GamePrefabs.RootCanvasPrefab);
            _entityContainer.RegisterEntity(root);
            return root;
        }

        public TopPanelView CreateTopPanel(Transform parent)
        {
            TopPanelView topPanelView = Object.Instantiate(_staticData.GamePrefabs.TopPanelViewPrefab, parent);
            topPanelView.Construct(_soundService, _entityContainer.GetEntity<SettingsView>());

            _entityContainer.RegisterEntity(new TopPanel(topPanelView, _stateSwitcher));
            return topPanelView;
        }

        public SettingsView CreateSettings(Transform parent)
        {
            SettingsView settingsView = Object.Instantiate(_staticData.GamePrefabs.SettingsViewPrefab, parent);
            settingsView.Construct(_persistentProgress.Progress.Settings, _soundService);
            _entityContainer.RegisterEntity(new SettingsPanel(settingsView, _soundService, _persistentProgress, _saveLoad));
            _entityContainer.RegisterEntity(settingsView);
            return settingsView;
        }

        public MainMenuView CreateMainMenu(Transform parent)
        {
            MainMenuView menuView = Object.Instantiate(_staticData.GamePrefabs.MainMenuViewPrefab, parent);
            menuView.Construct(_soundService);
            return menuView;
        }
    }
}