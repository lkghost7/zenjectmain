using UnityEngine;
using ZenjectMain.Code.Core.UI;
using ZenjectMain.Code.Core.UI.Gameplay.TopPanel;
using ZenjectMain.Code.Core.UI.MainMenu;
using ZenjectMain.Code.Core.UI.Settings;

namespace ZenjectMain.Code.Services.Factories.UIFactory
{
    public interface IUIFactory
    {
        RootCanvas CreateRootCanvas();
        MainMenuView CreateMainMenu(Transform parent);
        TopPanelView CreateTopPanel(Transform parent);
        SettingsView CreateSettings(Transform parent);
    }
}