using UnityEngine;
using ZenjectMain.Code.Core.UI.Settings;

namespace ZenjectMain.Code.Services.Factories.PersistentEntityFactory
{
    public interface IPersistentEntityFactory
    {
        SettingsView CreateSettings(Transform parent);
    }
}