using UnityEngine;
using ZenjectMain.Code.Core.UI.Settings;
using ZenjectMain.Code.Services.EntityContainer;
using ZenjectMain.Code.Services.PersistentProgress;
using ZenjectMain.Code.Services.SaveLoad;
using ZenjectMain.Code.Services.Sound;
using ZenjectMain.Code.Services.StaticData;

namespace ZenjectMain.Code.Services.Factories.PersistentEntityFactory
{
    public class PersistentEntityFactory : IPersistentEntityFactory
    {
        private readonly IEntityContainer _entityContainer;
        private readonly IPersistentProgress _persistentProgress;
        private readonly ISaveLoad _saveLoad;
        private readonly ISoundService _soundService;
        private readonly IStaticData _staticData;

        public PersistentEntityFactory(IEntityContainer entityContainer, IStaticData staticData, 
            IPersistentProgress persistentProgress, ISaveLoad saveLoad, ISoundService soundService)
        {
            _entityContainer = entityContainer;
            _persistentProgress = persistentProgress;
            _saveLoad = saveLoad;
            _soundService = soundService;
            _staticData = staticData;
        }
        
        public SettingsView CreateSettings(Transform parent)
        {
            SettingsView settingsView = Object.Instantiate(_staticData.GamePrefabs.SettingsViewPrefab, parent);
            settingsView.Construct(_persistentProgress.Progress.Settings, _soundService);
            _entityContainer.RegisterEntity(new SettingsPanel(settingsView, _soundService, _persistentProgress, _saveLoad));
            _entityContainer.RegisterEntity(settingsView);
            return settingsView;
        }
    }
}