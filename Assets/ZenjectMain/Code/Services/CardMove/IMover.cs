using System;
using UnityEngine;

namespace ZenjectMain.Code.Services.CardMove
{
    public interface IMover
    {
        void MoveItem(Transform item, Transform target, float time, float delay, Action callBack = null);
    }
}