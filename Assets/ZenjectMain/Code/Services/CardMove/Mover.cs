using System;
using DG.Tweening;
using UnityEngine;

namespace ZenjectMain.Code.Services.CardMove
{
    public class Mover : IMover
    {
        public void MoveItem(Transform item, Transform target, float time, float delay, Action callBack = null)
        {
            item.DOMove(target.position, time).SetEase(Ease.Linear).SetDelay(delay).OnComplete(() =>
            {
                callBack?.Invoke();
            });
        }
    }
}