using ZenjectMain.Code.Data.Progress;

namespace ZenjectMain.Code.Services.SaveLoad
{
    public interface ISaveLoad
    {
        void SaveProgress();
        PlayerProgress LoadProgress();
    }
}