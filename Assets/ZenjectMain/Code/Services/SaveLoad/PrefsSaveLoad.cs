using UnityEngine;
using ZenjectMain.Code.Data.Progress;
using ZenjectMain.Code.Extensions;
using ZenjectMain.Code.Services.PersistentProgress;

namespace ZenjectMain.Code.Services.SaveLoad
{
    public class PrefsSaveLoad : ISaveLoad
    {
        private readonly IPersistentProgress _playerProgress;
        private const string ProgressKey = "Progres";

        public PrefsSaveLoad(IPersistentProgress playerProgress) => _playerProgress = playerProgress;
        public void SaveProgress() => PlayerPrefs.SetString(ProgressKey, _playerProgress.Progress.ToJson());
        public PlayerProgress LoadProgress() => PlayerPrefs.GetString(ProgressKey)?.ToDeserialized<PlayerProgress>();
    }
}