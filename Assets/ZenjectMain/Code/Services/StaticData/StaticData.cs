using ZenjectMain.Code.Data.StaticData.Config;
using ZenjectMain.Code.Data.StaticData.Location;
using ZenjectMain.Code.Data.StaticData.Sounds;
using ZenjectMain.Code.Services.StaticData.StaticDataProvider;

namespace ZenjectMain.Code.Services.StaticData
{
    public class StaticData : IStaticData
    {
        public SoundData SoundData { get; private set; }
        public SettingsConfig SettingsConfig { get; private set; }
        public GamePrefabs GamePrefabs { get; private set; }
        public LocationData LocationData { get; private set; }

        private readonly IStaticDataProvider _staticDataProvider;

        public StaticData(IStaticDataProvider staticDataProvider)
        {
            _staticDataProvider = staticDataProvider;
            LoadStaticData();
        }

        private void LoadStaticData()
        {
            SettingsConfig = _staticDataProvider.LoadBlackJackSettingsConfig();
            GamePrefabs = _staticDataProvider.LoadBlackJackPrefabs();
            SoundData = _staticDataProvider.LoadSoundData();
            LocationData = _staticDataProvider.LoadLocationData();
        }
    }
}