using ZenjectMain.Code.Data.StaticData.Config;
using ZenjectMain.Code.Data.StaticData.Location;
using ZenjectMain.Code.Data.StaticData.Sounds;

namespace ZenjectMain.Code.Services.StaticData
{
    public interface IStaticData
    {
        SoundData SoundData { get; }
        LocationData LocationData { get; }
        SettingsConfig SettingsConfig { get; }
        GamePrefabs GamePrefabs { get; }
    }
}