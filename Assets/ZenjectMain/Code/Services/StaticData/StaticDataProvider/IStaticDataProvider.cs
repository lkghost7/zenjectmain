using ZenjectMain.Code.Data.StaticData.Config;
using ZenjectMain.Code.Data.StaticData.Location;
using ZenjectMain.Code.Data.StaticData.Sounds;

namespace ZenjectMain.Code.Services.StaticData.StaticDataProvider
{
    public interface IStaticDataProvider
    {
        SettingsConfig LoadBlackJackSettingsConfig();
        GamePrefabs LoadBlackJackPrefabs();
        SoundData LoadSoundData();
        LocationData LoadLocationData();
    }
}