using UnityEngine;
using ZenjectMain.Code.Data.StaticData.Config;
using ZenjectMain.Code.Data.StaticData.Location;
using ZenjectMain.Code.Data.StaticData.Sounds;

namespace ZenjectMain.Code.Services.StaticData.StaticDataProvider
{
    public class StaticDataProvider : IStaticDataProvider
    {
        private const string SettingsConfigPath = "StaticData/SettingsConfig";
        private const string GamePrefabsPath = "StaticData/GamePrefabs";
        private const string SoundDataPath = "StaticData/SoundData";
        private const string LocationDataPath = "StaticData/LocationData";

        public SettingsConfig LoadBlackJackSettingsConfig() =>
            Resources.Load<SettingsConfig>(SettingsConfigPath);

        public GamePrefabs LoadBlackJackPrefabs() =>
            Resources.Load<GamePrefabs>(GamePrefabsPath);

        public SoundData LoadSoundData() =>
            Resources.Load<SoundData>(SoundDataPath);
        
        public LocationData LoadLocationData() =>
            Resources.Load<LocationData>(LocationDataPath);
    }
}