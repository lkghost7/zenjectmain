using Plugins.Zenject.Source.Install;
using UnityEngine;
using ZenjectMain.Code.Services;
using ZenjectMain.Code.Services.CardMove;
using ZenjectMain.Code.Services.EntityContainer;
using ZenjectMain.Code.Services.Factories.GameFactory;
using ZenjectMain.Code.Services.Factories.PersistentEntityFactory;
using ZenjectMain.Code.Services.Factories.UIFactory;
using ZenjectMain.Code.Services.PersistentProgress;
using ZenjectMain.Code.Services.SaveLoad;
using ZenjectMain.Code.Services.SceneLoader;
using ZenjectMain.Code.Services.Sound;
using ZenjectMain.Code.Services.StaticData;
using ZenjectMain.Code.Services.StaticData.StaticDataProvider;
using ZenjectMain.Code.Services.UserBalance;

namespace ZenjectMain.Code.Infrastructure.Installers
{
    public class ServiceInstaller : MonoInstaller, ICoroutineRunner
    {
        [SerializeField] private SoundService _soundService;
        
        public override void InstallBindings()
        {
            RegisterSceneLoader();
            RegisterStaticDataProvider();
            RegisterCoroutineRunner();
            RegisterEntityContainer();
            RegisterSaveLoad();
            RegisterPersistentProgress();
            RegisterCardMoveService();
            RegisterStaticData();
            RegisterUserBalance();
            RegisterSoundService();
            RegisterPersistentEntityFactory();
            RegisterGameFactory();
            RegisterUIFactory();
        }

        private void RegisterSceneLoader() =>
            Container.Bind<ISceneLoader>().To<SceneLoader>().AsSingle();

        private void RegisterStaticDataProvider() =>
            Container.Bind<IStaticDataProvider>().To<StaticDataProvider>().AsSingle();

        private void RegisterCoroutineRunner() =>
            Container.Bind<ICoroutineRunner>().FromInstance(this).AsSingle();

        private void RegisterEntityContainer() =>
            Container.BindInterfacesTo<EntityContainer>().AsSingle();

        private void RegisterSaveLoad() =>
            Container.Bind<ISaveLoad>().To<PrefsSaveLoad>().AsSingle();

        private void RegisterPersistentProgress() =>
            Container.Bind<IPersistentProgress>().To<PersistentPlayerProgress>().AsSingle();        
        
        private void RegisterCardMoveService() =>
            Container.Bind<IMover>().To<Mover>().AsSingle();

        private void RegisterStaticData() =>
            Container.Bind<IStaticData>().To<StaticData>().AsSingle();

        private void RegisterUserBalance() =>
            Container.Bind<IUserBalance>().To<UserBalance>().AsSingle();

        private void RegisterSoundService() =>
            Container.Bind<ISoundService>().FromInstance(_soundService).AsSingle();

        private void RegisterPersistentEntityFactory() =>
            Container.Bind<IPersistentEntityFactory>().To<PersistentEntityFactory>().AsSingle();

        private void RegisterUIFactory() =>
            Container.Bind<IUIFactory>().To<UIFactory>().AsSingle();

        private void RegisterGameFactory() =>
            Container.Bind<IGameFactory>().To<GameFactory>().AsSingle();
    }
}