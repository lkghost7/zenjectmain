using UnityEngine;
using ZenjectMain.Code.Core.UI.MainMenu;
using ZenjectMain.Code.Core.UI.Settings;
using ZenjectMain.Code.Infrastructure.StateMachine.StateSwitcher;
using ZenjectMain.Code.Services.EntityContainer;
using ZenjectMain.Code.Services.Factories.UIFactory;
using ZenjectMain.Code.Services.SceneLoader;

namespace ZenjectMain.Code.Infrastructure.StateMachine.States
{
    public class MenuState : IState
    {
        private readonly IStateSwitcher _stateSwitcher;
        private readonly ISceneLoader _sceneLoader;
        private readonly IUIFactory _uiFactory;
        private readonly IEntityContainer _entityContainer;

        private const string MenuScene = "Menu";

        private MainMenuView _mainMenuView;

        public MenuState(IStateSwitcher stateSwitcher, ISceneLoader sceneLoader, IUIFactory uiFactory, IEntityContainer entityContainer)
        {
            _stateSwitcher = stateSwitcher;
            _sceneLoader = sceneLoader;
            _uiFactory = uiFactory;
            _entityContainer = entityContainer;
        }

        public void Enter()
        {
            Debug.Log("MenuState");
            _sceneLoader.LoadScene(MenuScene, InitializeUIElements);
        }

        public void Exit()
        {
            _mainMenuView.OnPlayButtonClick -= StartGameplay;
        }

        private void InitializeUIElements()
        {
            Transform rootCanvas = _uiFactory.CreateRootCanvas().transform;
            InitializeMainMenuView(rootCanvas);
        }

        private void InitializeMainMenuView(Transform rootCanvas)
        {
            _mainMenuView = _uiFactory.CreateMainMenu(rootCanvas);
            _uiFactory.CreateSettings(rootCanvas);
            _mainMenuView.SetSettingsPanel(_entityContainer.GetEntity<SettingsView>());
            _mainMenuView.OnPlayButtonClick += StartGameplay;
        }
 
        private void StartGameplay() =>
            _stateSwitcher.SwitchTo<LoadGameplayState>();
    }
}