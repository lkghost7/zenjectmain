using UnityEngine;
using ZenjectMain.Code.Infrastructure.StateMachine.StateSwitcher;
using ZenjectMain.Code.Services.CardMove;
using ZenjectMain.Code.Services.EntityContainer;
using ZenjectMain.Code.Services.Factories.GameFactory;
using ZenjectMain.Code.Services.Factories.UIFactory;
using ZenjectMain.Code.Services.SceneLoader;

namespace ZenjectMain.Code.Infrastructure.StateMachine.States
{
    public class LoadGameplayState : IState
    {
        private readonly IStateSwitcher _stateSwitcher;
        private readonly IUIFactory _uiFactory;
        private readonly IGameFactory _gameFactory;
        private readonly ISceneLoader _sceneLoader;
        private readonly IEntityContainer _entityContainer;

        private const string GameScene = "Game";

        public LoadGameplayState(IStateSwitcher stateSwitcher, IUIFactory uiFactory, 
            IGameFactory gameFactory, ISceneLoader sceneLoader, IEntityContainer entityContainer)
        {
            _stateSwitcher = stateSwitcher;
            _uiFactory = uiFactory;
            _gameFactory = gameFactory;
            _sceneLoader = sceneLoader;
            _entityContainer = entityContainer; 
        }
        
        public void Enter()
        {
            _sceneLoader.LoadScene(GameScene, CreateGame);
        }

        public void Exit()
        {
        }

        private void CreateGame()
        {
            Debug.Log("CreateGame");
            CreateGameplayComponents();
            CreateGameUI();
            // _stateSwitcher.SwitchTo<MyGameState>();
        }

        private void CreateGameplayComponents()
        {
            Mover mover = _gameFactory.CreateCardMover();
        }
 
        private void CreateGameUI()
        {
            Transform gameRoot = _uiFactory.CreateRootCanvas().transform;
            _uiFactory.CreateSettings(gameRoot);
            _uiFactory.CreateTopPanel(gameRoot);
        }
    }
}