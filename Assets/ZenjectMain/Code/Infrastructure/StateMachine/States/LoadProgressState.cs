using UnityEngine;
using ZenjectMain.Code.Data.Progress;
using ZenjectMain.Code.Infrastructure.StateMachine.StateSwitcher;
using ZenjectMain.Code.Services.PersistentProgress;
using ZenjectMain.Code.Services.SaveLoad;
using ZenjectMain.Code.Services.Sound;
using ZenjectMain.Code.Services.StaticData;

namespace ZenjectMain.Code.Infrastructure.StateMachine.States
{
    public class LoadProgressState : IState
    {
        private readonly IStateSwitcher _stateSwitcher;
        private readonly IPersistentProgress _playerProgress;
        private readonly ISaveLoad _saveLoadService;
        private readonly IStaticData _staticDataService;
        private readonly ISoundService _soundService;

        public LoadProgressState(IStateSwitcher stateSwitcher, IPersistentProgress playerProgress,
            ISaveLoad saveLoadService, IStaticData staticDataService, ISoundService soundService)
        {
            _staticDataService = staticDataService;
            _soundService = soundService;
            _saveLoadService = saveLoadService;
            _playerProgress = playerProgress;
            _stateSwitcher = stateSwitcher;
        }
        
        public void Enter()
        {
            Debug.Log("LoadProgressState");
            LoadProgressOrInitNew();
            InitializeSoundVolume();
            _stateSwitcher.SwitchTo<MenuState>();
        }

        public void Exit()
        {
        }
        
        private void LoadProgressOrInitNew()
        {
            _playerProgress.Progress = _saveLoadService.LoadProgress() ?? CreateNewProgress();
        }

        private PlayerProgress CreateNewProgress() => new PlayerProgress(_staticDataService.SettingsConfig.StartBalance);

        private void InitializeSoundVolume()
        {
            _soundService.Construct(_staticDataService.SoundData, _playerProgress.Progress.Settings);
            _soundService.PlayBackgroundMusic();
        }
    }
}