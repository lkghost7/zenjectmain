using System;

namespace ZenjectMain.Code.Data.Progress
{
    [Serializable]
    public class PlayerProgress
    {
        public Settings Settings;
        public int Balance;

        public PlayerProgress(int startBalance)
        {
            Balance = startBalance;
            Settings = new Settings();
        }
    }
}