using System;
using UnityEngine;
using ZenjectMain.Code.Data.Enums;

namespace ZenjectMain.Code.Data.StaticData.Sounds
{
    [Serializable]
    public class AudioClipData
    {
        public AudioClip Clip;
        public SoundId Id;
    }
}