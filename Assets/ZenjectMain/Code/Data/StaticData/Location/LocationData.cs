using UnityEngine;

namespace ZenjectMain.Code.Data.StaticData.Location
{
    [CreateAssetMenu(fileName = "Location Data", menuName = "Static Data/Location Data")]
    public class LocationData : ScriptableObject
    {
        [Header("Split Location")]
        public Location TestPosition;
    }
}