using UnityEngine;

namespace ZenjectMain.Code.Data.StaticData.Config
{
    [CreateAssetMenu(fileName = "SettingsConfig", menuName = "Static Data/SettingsConfig")]
    public class SettingsConfig : ScriptableObject
    {
        [Header("Data")]
        public int data;
        [Header("Balance")]
        public int StartBalance;
    }
}