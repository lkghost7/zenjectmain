using UnityEngine;
using ZenjectMain.Code.Core.UI;
using ZenjectMain.Code.Core.UI.Gameplay.TopPanel;
using ZenjectMain.Code.Core.UI.MainMenu;
using ZenjectMain.Code.Core.UI.Settings;

namespace ZenjectMain.Code.Data.StaticData.Config
{
    [CreateAssetMenu(fileName = "GamePrefabs Prefabs", menuName = "Static Data/GamePrefabs")]
    public class GamePrefabs : ScriptableObject
    {
        public RootCanvas RootCanvasPrefab;
        [Header("Settings")] 
        public SettingsView SettingsViewPrefab;
        public SoundSwitcher SoundSwitcherPrefab;
        [Header("Main Menu")]
        public MainMenuView MainMenuViewPrefab;
        [Header("Gameplay UI")]
        public TopPanelView TopPanelViewPrefab;
    }
}